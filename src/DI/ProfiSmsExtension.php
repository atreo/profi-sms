<?php

namespace Atreo\ProfiSms\DI;

use Nette\DI\CompilerExtension;
use Nette\Utils\Validators;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class ProfiSmsExtension extends CompilerExtension
{

	/**
	 * @var array
	 */
	private $defaults = [
		'sandbox' => TRUE,
	];



	public function loadConfiguration()
	{
		$config = $this->getConfig($this->defaults);

		Validators::assertField($config, 'login', 'string');
		Validators::assertField($config, 'password', 'string');
		Validators::assertField($config, 'sandbox', 'boolean');

		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('profiSms'))
			->setClass('Atreo\ProfiSms\ProfiSms', [$config]);

	}

}
