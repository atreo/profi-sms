<?php

namespace Atreo\ProfiSms;

use Nette\Object;
use Nette\Utils\Json;
use Tracy\Debugger;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class ProfiSms extends Object
{

	const URL = "http://api.profisms.cz";

	/**
	 * @var array
	 */
	private $config;



	/**
	 * @param array $config
	 */
	public function __construct(array $config)
	{
		$this->config = $config;
	}



	public function send($text, $receiverPhoneNumber, $senderPhoneNumber = NULL, \DateTime $sendAt = NULL)
	{
		$call = microtime(TRUE);

		if ($sendAt === NULL) {
			$sendAt = new \DateTime();
		}

		$data = [
			"CTRL" => $this->config['sandbox'] ? "test" : "sms",
			"_login" => $this->config['login'],
			"_service" => $this->config['sandbox'] ? "general" : "sms",
			"_call" => $call,
			"_password" => md5(md5($this->config['password']) . $call),
			"msisdn" => $receiverPhoneNumber,
			"text" => $text,
			"name" => "",
			"type" => "text",
			"split" => "concat",
			"delivery" => "0",
			"session" => $senderPhoneNumber ? $senderPhoneNumber : "",
			"senddate" => $sendAt->format("Y-m-d H:i:s"),
		];

		$url = self::URL . "?" . http_build_query($data);

		$result = join("", file($url));

		$json = Json::decode($result);

		Debugger::log($json, 'profi-sms');

		if (!isset($json->error) || !isset($json->error->message) || $json->error->message != 'OK') {
			$message = 'Message not sent.';
			$code = '0';
			if (isset($json->error) && isset($json->error->message)) {
				$message = $json->error->message;
			}
			if (isset($json->error) && isset($json->error->code)) {
				$code = $json->error->code;
			}
			throw new ProfiSmsException($message . ' | CODE: ' . $code);
		}

		return TRUE;
	}

}
